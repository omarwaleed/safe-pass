import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import PouchDB from 'pouchdb';

import Login from './Login.jsx';
import SignUp from './SignUp.jsx';
import Main from './Main.jsx';

const db = new PouchDB('users');

class App extends Component {
	constructor(){
		super();
		this.state = {
			currentView: null
		}

		db.createIndex({
			index: {
				fields: ["username"]
			}
		})
	}
	componentDidMount(){
		// this.setState({currentView: <Login />})
		this.changeView('login');
	}
	changeView = (view)=>{

		const getViewWithProps = (View) => {
			return this.setState({currentView: <View changeView={this.changeView} />})
		}
		switch(view){
			case 'login': getViewWithProps(Login); break;
			case 'signup': getViewWithProps(SignUp); break;
			case 'main': getViewWithProps(Main); break;
			default: this.setState({currentView: null})
		}
	}
	render(){
		return(
			<div style={{height: '100%', width: '100%'}}>
				{this.state.currentView}
			</div>
		)
	}
}

ReactDOM.render(<App />, document.getElementById("root"));