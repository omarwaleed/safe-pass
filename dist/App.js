process.env.HMR_PORT=38085;process.env.HMR_HOSTNAME="localhost";// modules are defined as an array
// [ module function, map of requires ]
//
// map of requires is short require name -> numeric require
//
// anything defined in a previous bundle is accessed via the
// orig method which is the require for previous bundles

// eslint-disable-next-line no-global-assign
parcelRequire = (function (modules, cache, entry) {
  // Save the require from previous bundle to this closure if any
  var previousRequire = typeof parcelRequire === 'function' && parcelRequire;
  var nodeRequire = typeof require === 'function' && require;

  function newRequire(name, jumped) {
    if (!cache[name]) {
      if (!modules[name]) {
        // if we cannot find the module within our internal map or
        // cache jump to the current global require ie. the last bundle
        // that was added to the page.
        var currentRequire = typeof parcelRequire === 'function' && parcelRequire;
        if (!jumped && currentRequire) {
          return currentRequire(name, true);
        }

        // If there are other bundles on this page the require from the
        // previous one is saved to 'previousRequire'. Repeat this as
        // many times as there are bundles until the module is found or
        // we exhaust the require chain.
        if (previousRequire) {
          return previousRequire(name, true);
        }

        // Try the node require function if it exists.
        if (nodeRequire && typeof name === 'string') {
          return nodeRequire(name);
        }

        var err = new Error('Cannot find module \'' + name + '\'');
        err.code = 'MODULE_NOT_FOUND';
        throw err;
      }

      localRequire.resolve = resolve;

      var module = cache[name] = new newRequire.Module(name);

      modules[name][0].call(module.exports, localRequire, module, module.exports);
    }

    return cache[name].exports;

    function localRequire(x){
      return newRequire(localRequire.resolve(x));
    }

    function resolve(x){
      return modules[name][1][x] || x;
    }
  }

  function Module(moduleName) {
    this.id = moduleName;
    this.bundle = newRequire;
    this.exports = {};
  }

  newRequire.isParcelRequire = true;
  newRequire.Module = Module;
  newRequire.modules = modules;
  newRequire.cache = cache;
  newRequire.parent = previousRequire;

  for (var i = 0; i < entry.length; i++) {
    newRequire(entry[i]);
  }

  // Override the current require with this new one
  return newRequire;
})({18:[function(require,module,exports) {
'use strict';

var _crypto = require('crypto');

var _crypto2 = _interopRequireDefault(_crypto);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

module.exports = {
	encrypt: function (plainText, password) {
		const cipher = _crypto2.default.createCipher("aes192", password);
		let encrypted = cipher.update(plainText, 'utf8', 'hex');
		encrypted += cipher.final('hex');
		return encrypted;
	},
	decrypt: function (encrypted, password) {
		const decipher = _crypto2.default.createDecipher("aes192", password);
		let decrypted = decipher.update(encrypted, 'hex', 'utf8');
		decrypted += decipher.final('utf8');
		return decrypted;
	}
};
},{}],3:[function(require,module,exports) {
'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _bcrypt = require('bcrypt');

var _bcrypt2 = _interopRequireDefault(_bcrypt);

var _randomstring = require('randomstring');

var _randomstring2 = _interopRequireDefault(_randomstring);

var _pouchdb = require('pouchdb');

var _pouchdb2 = _interopRequireDefault(_pouchdb);

var _pouchdbFind = require('pouchdb-find');

var _pouchdbFind2 = _interopRequireDefault(_pouchdbFind);

var _encryption = require('./encryption.js');

var encryption = _interopRequireWildcard(_encryption);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

_pouchdb2.default.plugin(_pouchdbFind2.default);

const db = new _pouchdb2.default('users');

class Login extends _react.Component {
	constructor() {
		super();

		this.handleSubmit = async () => {
			try {
				let result = await db.find({
					selector: {
						username: this.state.username
					},
					limit: 1
				});
				if (result.docs.length > 0) {
					const same = await _bcrypt2.default.compare(this.state.password, result.docs[0].password).catch(err => {
						this.setState({ error: "Password incorrect" });throw Error("Incorrect password");
					});
					if (!same) {
						return this.setState({ error: "Invalid credentials" });
					}
					const sessionKey = _randomstring2.default.generate(10);
					sessionStorage.setItem('sKey', sessionKey);
					const encryptedPassword = encryption.encrypt(this.state.password, sessionKey);
					sessionStorage.setItem('encP', encryptedPassword);
					sessionStorage.setItem('userID', result.docs[0]._id);
					this.props.changeView('main');
				} else {
					this.setState({ error: "Invalid credentials" });
				}
			} catch (err) {
				console.error(err);
				this.setState({ error: "Something went wrong" });
			}
		};

		this.handleKeyPress = event => {
			if (event.key == 'Enter') {
				this.handleSubmit();
			}
		};

		this.state = {
			username: "",
			password: "",
			error: ""
		};
	}

	render() {
		const loginInput = {
			flex: 1,
			borderRadius: 3,
			marginBottom: 10,
			padding: 5,
			borderColor: '#eee',
			borderWidth: 1
		};
		const alternativeViewStyle = {
			fontFamily: 'sans-serif',
			alignSelf: 'center',
			marginTop: 10,
			textDecoration: 'none',
			color: 'green'
		};
		return _react2.default.createElement(
			'div',
			{ style: {
					width: '100%',
					height: '100%',
					display: 'flex',
					flexDirection: 'column',
					justifyContent: 'center',
					alignItems: 'center'
				} },
			this.state.error ? _react2.default.createElement(
				'div',
				{ style: {
						width: 200,
						color: 'red',
						border: '1px solid red',
						textAlign: 'center',
						padding: 10,
						marginBottom: 10,
						borderRadius: 3
					} },
				this.state.error
			) : null,
			_react2.default.createElement(
				'div',
				{ style: { display: 'flex', flexDirection: 'column', width: 350 } },
				_react2.default.createElement('input', { style: loginInput, onKeyPress: this.handleKeyPress, value: this.state.username, type: 'text', placeholder: 'Username', onChange: event => this.setState({ username: event.currentTarget.value }) }),
				_react2.default.createElement('input', { style: loginInput, onKeyPress: this.handleKeyPress, value: this.state.password, type: 'password', placeholder: 'Password', onChange: event => this.setState({ password: event.currentTarget.value }) }),
				_react2.default.createElement(
					'button',
					{
						style: {
							fontSize: 18,
							color: '#fff',
							backgroundColor: 'rgba(92, 149, 242, 0.9)',
							borderRadius: 3,
							borderWidth: 1,
							padding: 5,
							width: 200,
							alignSelf: 'center'
						},
						onClick: this.handleSubmit
					},
					'Login'
				),
				_react2.default.createElement(
					'a',
					{ href: '#', style: alternativeViewStyle, onClick: () => this.props.changeView('signup') },
					'Sign Up'
				)
			)
		);
	}
}
exports.default = Login;
},{"./encryption.js":18}],20:[function(require,module,exports) {
'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _bcrypt = require('bcrypt');

var _bcrypt2 = _interopRequireDefault(_bcrypt);

var _randomstring = require('randomstring');

var _randomstring2 = _interopRequireDefault(_randomstring);

var _pouchdb = require('pouchdb');

var _pouchdb2 = _interopRequireDefault(_pouchdb);

var _pouchdbFind = require('pouchdb-find');

var _pouchdbFind2 = _interopRequireDefault(_pouchdbFind);

var _encryption = require('./encryption.js');

var encryption = _interopRequireWildcard(_encryption);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

_pouchdb2.default.plugin(_pouchdbFind2.default);

const db = new _pouchdb2.default('users');

class SignUp extends _react.Component {
	constructor() {
		super();

		this.handleSubmit = async () => {
			if (!(this.state.username && this.state.password && this.state.password.length >= 6)) {
				return this.setState({ error: "Credentials not allowed. Please check username and password" });
			}
			try {
				let oldEntries = await db.find({
					selector: {
						username: this.state.username
					}

				});
				console.log(oldEntries);
				if (oldEntries.docs.length > 0) {
					return this.setState({ error: "Username already exists" });
				}
				const result = await db.post({
					username: this.state.username,
					password: await _bcrypt2.default.hash(this.state.password, 11)
				});
				console.log(result);
				const sessionKey = _randomstring2.default.generate(10);
				sessionStorage.setItem('sKey', sessionKey);
				const encryptedPassword = encryption.encrypt(this.state.password, sessionKey);
				sessionStorage.setItem('encP', encryptedPassword);
				sessionStorage.setItem('userID', result.id);
				this.props.changeView('main');
			} catch (err) {
				console.error(err);
				this.setState({ error: "Something went wrong" });
			}
		};

		this.handleKeyPress = event => {
			if (event.key == 'Enter') {
				this.handleSubmit();
			}
		};

		this.state = {
			username: "",
			password: "",
			error: ""
		};
	}

	render() {
		const loginInput = {
			flex: 1,
			borderRadius: 3,
			marginBottom: 10,
			padding: 5,
			borderColor: '#eee',
			borderWidth: 1
		};
		const alternativeViewStyle = {
			fontFamily: 'sans-serif',
			alignSelf: 'center',
			marginTop: 10,
			textDecoration: 'none',
			color: 'green'
		};
		return _react2.default.createElement(
			'div',
			{ style: {
					width: '100%',
					height: '100%',
					display: 'flex',
					flexDirection: 'column',
					justifyContent: 'center',
					alignItems: 'center'
				} },
			this.state.error ? _react2.default.createElement(
				'div',
				{ style: {
						width: 200,
						color: 'red',
						border: '1px solid red',
						textAlign: 'center',
						padding: 10,
						marginBottom: 10,
						borderRadius: 3
					} },
				this.state.error
			) : null,
			_react2.default.createElement(
				'div',
				{ style: { display: 'flex', flexDirection: 'column', width: 350 } },
				_react2.default.createElement('input', { style: loginInput, onKeyPress: this.handleKeyPress, value: this.state.username, type: 'text', placeholder: 'Username', onChange: event => this.setState({ username: event.currentTarget.value }) }),
				_react2.default.createElement('input', { style: loginInput, onKeyPress: this.handleKeyPress, value: this.state.password, type: 'password', placeholder: 'Password (At least 6 characters)', onChange: event => this.setState({ password: event.currentTarget.value }) }),
				_react2.default.createElement(
					'button',
					{
						style: {
							fontSize: 18,
							color: '#fff',
							backgroundColor: 'rgba(92, 149, 242, 0.9)',
							borderRadius: 3,
							borderWidth: 1,
							padding: 5,
							width: 200,
							alignSelf: 'center'
						},
						onClick: this.handleSubmit
					},
					'Sign Up'
				),
				_react2.default.createElement(
					'a',
					{ href: '#', style: alternativeViewStyle, onClick: () => this.props.changeView('login') },
					'Log In'
				)
			)
		);
	}
}
exports.default = SignUp;
},{"./encryption.js":18}],21:[function(require,module,exports) {
'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _pouchdb = require('pouchdb');

var _pouchdb2 = _interopRequireDefault(_pouchdb);

var _pouchdbFind = require('pouchdb-find');

var _pouchdbFind2 = _interopRequireDefault(_pouchdbFind);

var _copyToClipboard = require('copy-to-clipboard');

var _copyToClipboard2 = _interopRequireDefault(_copyToClipboard);

var _encryption = require('./encryption.js');

var encryption = _interopRequireWildcard(_encryption);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

_pouchdb2.default.plugin(_pouchdbFind2.default);


const db = new _pouchdb2.default('passwords');

class Main extends _react.Component {
	constructor() {
		super();

		this.signOut = () => {
			sessionStorage.clear();
			this.props.changeView('login');
		};

		this.handleSubmit = async () => {
			if (!(this.state.newLabel && this.state.newPassword)) {
				return this.setState({ newError: "Password is missing label or password value" });
			}
			try {
				const creation = await db.post({
					label: this.state.newLabel,
					password: encryption.encrypt(this.state.newPassword, encryption.decrypt(sessionStorage.getItem('encP'), sessionStorage.getItem('sKey'))),
					user: sessionStorage.getItem('userID')
				});
				console.log(creation);
				this.componentDidMount();
				this.setState({ newLabel: "", newPassword: "" });
			} catch (err) {
				console.error(err);
				this.setState({ error: "Something happened while creating your password" });
			}
		};

		this.handleDelete = doc => {
			db.remove(doc).then(result => {
				console.log(result);
				this.componentDidMount();
			}).catch(err => {
				console.error(err);
				this.setState({ error: "Deletion for password " + doc.label + " failed" });
			});
		};

		this.state = {
			passwords: [],
			error: "",
			showIndex: -1,
			newError: "",
			newLabel: "",
			newPassword: ""
		};
	}

	componentDidMount() {
		db.find({
			selector: {
				user: sessionStorage.getItem('userID')
			}
		}).then(result => {
			this.setState({ passwords: result.docs });
		}).catch(err => {
			console.log(err);
			this.setState({ error: "Something went wrong retrieving your passwords" });
		});
	}

	render() {
		return _react2.default.createElement(
			'div',
			{ style: { display: 'flex', flexDirection: 'column' } },
			_react2.default.createElement(
				'div',
				{ style: { width: '100%' } },
				_react2.default.createElement(
					'button',
					{ onClick: this.signOut },
					'Sign Out'
				)
			),
			_react2.default.createElement('div', { style: {
					display: 'none'
				}, id: 'clip' }),
			_react2.default.createElement(
				'div',
				null,
				this.state.newError ? _react2.default.createElement(
					'div',
					{
						style: {
							width: 200,
							color: 'red',
							border: '1px solid red',
							textAlign: 'center',
							padding: 10,
							marginBottom: 10,
							borderRadius: 3
						}
					},
					this.state.newError
				) : null,
				_react2.default.createElement('input', { type: 'text', placeholder: 'Label', value: this.state.newLabel, onChange: event => this.setState({ newLabel: event.currentTarget.value }) }),
				_react2.default.createElement('input', { type: 'password', placeholder: 'Password', value: this.state.newPassword, onChange: event => this.setState({ newPassword: event.currentTarget.value }) }),
				_react2.default.createElement(
					'button',
					{ onClick: this.handleSubmit },
					'Create Password'
				)
			),
			_react2.default.createElement(
				'div',
				null,
				_react2.default.createElement(
					'table',
					null,
					_react2.default.createElement(
						'thead',
						null,
						_react2.default.createElement(
							'tr',
							null,
							_react2.default.createElement(
								'td',
								null,
								this.state.passwords.length
							)
						),
						_react2.default.createElement(
							'tr',
							null,
							_react2.default.createElement(
								'td',
								null,
								'Name'
							),
							_react2.default.createElement(
								'td',
								null,
								'Password'
							),
							_react2.default.createElement('td', null)
						)
					),
					_react2.default.createElement(
						'tbody',
						null,
						this.state.passwords.map((password, index) => {
							return _react2.default.createElement(
								'tr',
								{ key: "password-" + index },
								_react2.default.createElement(
									'td',
									null,
									password.label
								),
								_react2.default.createElement(
									'td',
									null,
									index === this.state.showIndex ? _react2.default.createElement(
										'div',
										{ onClick: event => {
												(0, _copyToClipboard2.default)(event.currentTarget.innerText);
											}
										},
										encryption.decrypt(password.password, encryption.decrypt(sessionStorage.getItem('encP'), sessionStorage.getItem('sKey')))
									) : "***********"
								),
								_react2.default.createElement(
									'td',
									null,
									index === this.state.showIndex ? _react2.default.createElement(
										'button',
										{ onClick: () => this.setState({ showIndex: -1 }) },
										'Hide'
									) : _react2.default.createElement(
										'button',
										{ onClick: () => this.setState({ showIndex: index }) },
										'Show'
									),
									_react2.default.createElement(
										'button',
										{ onClick: () => this.handleDelete(password) },
										'Delete'
									)
								)
							);
						})
					)
				)
			)
		);
	}
}
exports.default = Main;
},{"./encryption.js":18}],1:[function(require,module,exports) {
'use strict';

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactDom = require('react-dom');

var _reactDom2 = _interopRequireDefault(_reactDom);

var _pouchdb = require('pouchdb');

var _pouchdb2 = _interopRequireDefault(_pouchdb);

var _Login = require('./Login.jsx');

var _Login2 = _interopRequireDefault(_Login);

var _SignUp = require('./SignUp.jsx');

var _SignUp2 = _interopRequireDefault(_SignUp);

var _Main = require('./Main.jsx');

var _Main2 = _interopRequireDefault(_Main);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const db = new _pouchdb2.default('users');

class App extends _react.Component {
	constructor() {
		super();

		this.changeView = view => {

			const getViewWithProps = View => {
				return this.setState({ currentView: _react2.default.createElement(View, { changeView: this.changeView }) });
			};
			switch (view) {
				case 'login':
					getViewWithProps(_Login2.default);break;
				case 'signup':
					getViewWithProps(_SignUp2.default);break;
				case 'main':
					getViewWithProps(_Main2.default);break;
				default:
					this.setState({ currentView: null });
			}
		};

		this.state = {
			currentView: null
		};

		db.createIndex({
			index: {
				fields: ["username"]
			}
		});
	}
	componentDidMount() {
		// this.setState({currentView: <Login />})
		this.changeView('login');
	}

	render() {
		return _react2.default.createElement(
			'div',
			{ style: { height: '100%', width: '100%' } },
			this.state.currentView
		);
	}
}

_reactDom2.default.render(_react2.default.createElement(App, null), document.getElementById("root"));
},{"./Login.jsx":3,"./SignUp.jsx":20,"./Main.jsx":21}],25:[function(require,module,exports) {
var OVERLAY_ID = '__parcel__error__overlay__';

var global = (1, eval)('this');
var OldModule = module.bundle.Module;

function Module(moduleName) {
  OldModule.call(this, moduleName);
  this.hot = {
    data: module.bundle.hotData,
    _acceptCallbacks: [],
    _disposeCallbacks: [],
    accept: function (fn) {
      this._acceptCallbacks.push(fn || function () {});
    },
    dispose: function (fn) {
      this._disposeCallbacks.push(fn);
    }
  };

  module.bundle.hotData = null;
}

module.bundle.Module = Module;

var parent = module.bundle.parent;
if ((!parent || !parent.isParcelRequire) && typeof WebSocket !== 'undefined') {
  var hostname = process.env.HMR_HOSTNAME || location.hostname;
  var protocol = location.protocol === 'https:' ? 'wss' : 'ws';
  var ws = new WebSocket(protocol + '://' + hostname + ':' + process.env.HMR_PORT + '/');
  ws.onmessage = function(event) {
    var data = JSON.parse(event.data);

    if (data.type === 'update') {
      data.assets.forEach(function (asset) {
        hmrApply(global.parcelRequire, asset);
      });

      data.assets.forEach(function (asset) {
        if (!asset.isNew) {
          hmrAccept(global.parcelRequire, asset.id);
        }
      });
    }

    if (data.type === 'reload') {
      ws.close();
      ws.onclose = function () {
        location.reload();
      }
    }

    if (data.type === 'error-resolved') {
      console.log('[parcel] ✨ Error resolved');

      removeErrorOverlay();
    }

    if (data.type === 'error') {
      console.error('[parcel] 🚨  ' + data.error.message + '\n' + data.error.stack);

      removeErrorOverlay();

      var overlay = createErrorOverlay(data);
      document.body.appendChild(overlay);
    }
  };
}

function removeErrorOverlay() {
  var overlay = document.getElementById(OVERLAY_ID);
  if (overlay) {
    overlay.remove();
  }
}

function createErrorOverlay(data) {
  var overlay = document.createElement('div');
  overlay.id = OVERLAY_ID;

  // html encode message and stack trace
  var message = document.createElement('div');
  var stackTrace = document.createElement('pre');
  message.innerText = data.error.message;
  stackTrace.innerText = data.error.stack;

  overlay.innerHTML = (
    '<div style="background: black; font-size: 16px; color: white; position: fixed; height: 100%; width: 100%; top: 0px; left: 0px; padding: 30px; opacity: 0.85; font-family: Menlo, Consolas, monospace; z-index: 9999;">' +
      '<span style="background: red; padding: 2px 4px; border-radius: 2px;">ERROR</span>' +
      '<span style="top: 2px; margin-left: 5px; position: relative;">🚨</span>' +
      '<div style="font-size: 18px; font-weight: bold; margin-top: 20px;">' + message.innerHTML + '</div>' +
      '<pre>' + stackTrace.innerHTML + '</pre>' +
    '</div>'
  );

  return overlay;

}

function getParents(bundle, id) {
  var modules = bundle.modules;
  if (!modules) {
    return [];
  }

  var parents = [];
  var k, d, dep;

  for (k in modules) {
    for (d in modules[k][1]) {
      dep = modules[k][1][d];
      if (dep === id || (Array.isArray(dep) && dep[dep.length - 1] === id)) {
        parents.push(+k);
      }
    }
  }

  if (bundle.parent) {
    parents = parents.concat(getParents(bundle.parent, id));
  }

  return parents;
}

function hmrApply(bundle, asset) {
  var modules = bundle.modules;
  if (!modules) {
    return;
  }

  if (modules[asset.id] || !bundle.parent) {
    var fn = new Function('require', 'module', 'exports', asset.generated.js);
    asset.isNew = !modules[asset.id];
    modules[asset.id] = [fn, asset.deps];
  } else if (bundle.parent) {
    hmrApply(bundle.parent, asset);
  }
}

function hmrAccept(bundle, id) {
  var modules = bundle.modules;
  if (!modules) {
    return;
  }

  if (!modules[id] && bundle.parent) {
    return hmrAccept(bundle.parent, id);
  }

  var cached = bundle.cache[id];
  bundle.hotData = {};
  if (cached) {
    cached.hot.data = bundle.hotData;
  }

  if (cached && cached.hot && cached.hot._disposeCallbacks.length) {
    cached.hot._disposeCallbacks.forEach(function (cb) {
      cb(bundle.hotData);
    });
  }

  delete bundle.cache[id];
  bundle(id);

  cached = bundle.cache[id];
  if (cached && cached.hot && cached.hot._acceptCallbacks.length) {
    cached.hot._acceptCallbacks.forEach(function (cb) {
      cb();
    });
    return true;
  }

  return getParents(global.parcelRequire, id).some(function (id) {
    return hmrAccept(global.parcelRequire, id)
  });
}

},{}]},{},[25,1])
//# sourceMappingURL=/App.map