import React, {Component} from 'react';

import PouchDB from 'pouchdb';
import PouchDBFind from 'pouchdb-find';
PouchDB.plugin(PouchDBFind);
import Copy from 'copy-to-clipboard';

import * as encryption from './encryption.js';

const db = new PouchDB('passwords');


export default class Main extends Component{
	constructor(){
		super();
		this.state = {
			passwords: [],
			error: "",
			showIndex: -1,
			newError: "",
			newLabel: "",
			newPassword: ""
		}
	}

	signOut = ()=>{
		sessionStorage.clear();
		this.props.changeView('login');
	}

	componentDidMount(){
		db.find({
			selector: {
				user: sessionStorage.getItem('userID')
			}
		})
		.then((result)=>{
			this.setState({passwords: result.docs})
		})
		.catch((err)=>{
			console.log(err);
			this.setState({error: "Something went wrong retrieving your passwords"})
		})
	}

	handleSubmit = async ()=>{
		if(!(this.state.newLabel && this.state.newPassword)){
			return this.setState({newError: "Password is missing label or password value"})
		}
		try {
			const creation = await db.post({
				label: this.state.newLabel,
				password: encryption.encrypt(this.state.newPassword, encryption.decrypt(sessionStorage.getItem('encP'), sessionStorage.getItem('sKey'))),
				user: sessionStorage.getItem('userID')
			})
			console.log(creation)
			this.componentDidMount();
			this.setState({newLabel: "", newPassword: ""})
		} catch (err){
			console.error(err);
			this.setState({error: "Something happened while creating your password"});
		}
	}

	handleDelete = (doc)=>{
		db.remove(doc)
		.then((result)=>{
			console.log(result);
			this.componentDidMount();
		})
		.catch((err)=>{
			console.error(err);
			this.setState({error: "Deletion for password "+doc.label+" failed"})
		})
	}

	render(){
		return(
			<div style={{display: 'flex', flexDirection: 'column'}}>
				<div style={{width: '100%'}}>
					<button onClick={this.signOut}>Sign Out</button>
				</div>
				<div style={{
					display: 'none'
				}} id="clip">
				</div>
				<div>
					{
						this.state.newError?
						<div
						style={{
							width: 200,
							color: 'red',
							border: '1px solid red',
							textAlign: 'center',
							padding: 10,
							marginBottom: 10,
							borderRadius: 3
						}}
						>
							{this.state.newError}
						</div>
						:
						null
					}
					<input type="text" placeholder="Label" value={this.state.newLabel} onChange={(event)=>this.setState({newLabel: event.currentTarget.value})} />
					<input type="password" placeholder="Password" value={this.state.newPassword} onChange={(event)=>this.setState({newPassword: event.currentTarget.value})} />
					<button onClick={this.handleSubmit}>Create Password</button>
				</div>
				<div>
					<table>
						<thead>
							<tr>
								<td>{this.state.passwords.length}</td>
							</tr>
							<tr>
								<td>Name</td>
								<td>Password</td>
								<td></td>
							</tr>
						</thead>
						<tbody>
							{
								this.state.passwords.map((password, index)=>{
									return(
										<tr key={"password-"+index}>
											<td>{password.label}</td>
											<td>{(index === this.state.showIndex)? 
														<div onClick={(event)=>{
															Copy(event.currentTarget.innerText)
														}}
														>
															{
																encryption.decrypt(password.password,
																			encryption.decrypt(sessionStorage.getItem('encP')
																												, sessionStorage.getItem('sKey'))
																								)
															}
														</div>
													: "***********"
													}</td>
											<td>
												{(index === this.state.showIndex)?
												<button onClick={()=>this.setState({showIndex: -1})}>Hide</button>
												:
												<button onClick={()=>this.setState({showIndex: index})}>Show</button>
												}
												<button onClick={()=>this.handleDelete(password)}>Delete</button>
											</td>
										</tr>
									)
								})
							}
						</tbody>
					</table>
				</div>
			</div>
		)
	}
}