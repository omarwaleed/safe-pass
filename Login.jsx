import React, { Component } from 'react';
import bcrypt from 'bcrypt';
import randomstring from 'randomstring';
import PouchDB from 'pouchdb';
import PouchDBFind from 'pouchdb-find';
PouchDB.plugin(PouchDBFind);

import * as encryption from './encryption.js';

const db = new PouchDB('users');

export default class Login extends Component {
	constructor(){
		super();
		this.state = {
			username: "",
			password: "",
			error: ""
		}

	}

	handleSubmit = async ()=>{
		try {
			let result = await db.find({
				selector: {
					username: this.state.username
				},
				limit: 1
			})
			if(result.docs.length > 0){
				const same = await bcrypt.compare(this.state.password, result.docs[0].password).catch((err)=>{this.setState({error: "Password incorrect"}); throw Error("Incorrect password")})
				if(!same){
					return this.setState({error: "Invalid credentials"})
				}
				const sessionKey = randomstring.generate(10);
				sessionStorage.setItem('sKey', sessionKey);
				const encryptedPassword = encryption.encrypt(this.state.password, sessionKey)
				sessionStorage.setItem('encP', encryptedPassword)
				sessionStorage.setItem('userID', result.docs[0]._id)
				this.props.changeView('main');
			} else {
				this.setState({error: "Invalid credentials"})
			}
		} catch (err){
			console.error(err);
			this.setState({error: "Something went wrong"})
		}
	}

	handleKeyPress = (event) => {
		if(event.key == 'Enter'){
			this.handleSubmit();
		}
	}

	render(){
		const loginInput = {
			flex: 1,
			borderRadius: 3,
			marginBottom: 10,
			padding: 5,
			borderColor: '#eee',
			borderWidth: 1
		};
		const alternativeViewStyle = {
			fontFamily: 'sans-serif',
			alignSelf: 'center',
			marginTop: 10,
			textDecoration: 'none',
			color: 'green',
		}
		return(
			<div style={{
				width: '100%',
				height: '100%',
				display: 'flex',
				flexDirection: 'column',
				justifyContent: 'center',
				alignItems: 'center'
			}}>
				{
					(this.state.error)?
					<div style={{
						width: 200,
						color: 'red',
						border: '1px solid red',
						textAlign: 'center',
						padding: 10,
						marginBottom: 10,
						borderRadius: 3
					}}>
						{this.state.error}
					</div>
					:
					null
				}
				<div style={{display: 'flex', flexDirection: 'column', width: 350}}>
					<input style={loginInput} onKeyPress={this.handleKeyPress} value={this.state.username} type="text" placeholder="Username" onChange={(event)=>this.setState({username: event.currentTarget.value})} />
					<input style={loginInput} onKeyPress={this.handleKeyPress} value={this.state.password} type="password" placeholder="Password" onChange={(event)=>this.setState({password: event.currentTarget.value})} />
					<button 
					style={{
						fontSize: 18,
						color: '#fff',
						backgroundColor: 'rgba(92, 149, 242, 0.9)',
						borderRadius: 3,
						borderWidth: 1,
						padding: 5,
						width: 200,
						alignSelf: 'center'
					}}
					onClick={this.handleSubmit}
					>Login</button>
					<a href="#" style={alternativeViewStyle} onClick={()=>this.props.changeView('signup')} >Sign Up</a>
				</div>
			</div>
		)
	}
}