import crypto from 'crypto';

module.exports = {
	encrypt: function(plainText, password){
		const cipher = crypto.createCipher("aes192", password);
		let encrypted = cipher.update(plainText, 'utf8', 'hex');
		encrypted += cipher.final('hex');
		return encrypted;
	},
	decrypt: function(encrypted, password){
		const decipher = crypto.createDecipher("aes192", password);
		let decrypted = decipher.update(encrypted, 'hex', 'utf8');
		decrypted += decipher.final('utf8');
		return decrypted;
	}
}